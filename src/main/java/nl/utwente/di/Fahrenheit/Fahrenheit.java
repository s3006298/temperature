package nl.utwente.di.Fahrenheit;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets a Celsius number and returns the book price
 */

public class Fahrenheit extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ToFahrenheit toFahrenheit;
	
    public void init() throws ServletException {
    	toFahrenheit = new ToFahrenheit();
    }

	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Fahrenheit";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius: " +
                   request.getParameter("Celsius") + "\n" +
                "  <P>Fahrenheit: " +
                   toFahrenheit.getFahrenheit(Double.valueOf(request.getParameter("Celsius"))) +
                "</BODY></HTML>");
  }
}
